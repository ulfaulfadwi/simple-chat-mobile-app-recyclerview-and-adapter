# Ulva Dewiyanti
## Simple Chats App Using Recycler View and Adapter

This is a project for Global Learning System assignment to build a simple chat app using RecyclerView and Adapter.

Below is the result of my code.

![Untitled](/uploads/db29ec9ec19d708d3902668afff5882a/Untitled.png)

All the drawable assets that I used in the code is from [freepik.com](www.freepik.com)