package com.example.chatapps;

public class Chats {
    private String name;
    private String msg;
    private int img;

    public String getName() {
        return name;
    }

    public String getMsg() {
        return msg;
    }

    public int getImg() {
        return img;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
