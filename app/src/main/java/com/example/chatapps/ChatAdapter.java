package com.example.chatapps;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    Context ctx;
    ArrayList<Chats> listChat;

    public ChatAdapter(Context ctx, ArrayList<Chats> listChat){
        this.ctx = ctx;
        this.listChat = listChat;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.list_item, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Chats chats = listChat.get(position);

        holder.img_profile.setImageResource(chats.getImg());
        holder.textName.setText(chats.getName());
        holder.textMsg.setText(chats.getMsg());

    }

    @Override
    public int getItemCount() {
        return listChat.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textName, textMsg;
        ImageView img_profile;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textName = itemView.findViewById(R.id.nama_orang);
            textMsg = itemView.findViewById(R.id.isi_pesan);
            img_profile = itemView.findViewById(R.id.icon_chat);

        }


    }
}
