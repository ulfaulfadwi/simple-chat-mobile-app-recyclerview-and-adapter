package com.example.chatapps;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    RecyclerView rv;
    ChatAdapter chatAdapter;
    RecyclerView.LayoutManager layoutManager;
    ArrayList<Chats> array_chat = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        Chats chat1 = new Chats();
        chat1.setName("James");
        chat1.setMsg("Thank you! That was very helpful!");
        chat1.setImg(R.drawable.james);

        Chats chat2 = new Chats();
        chat2.setName("Will Kenny");
        chat2.setMsg("I know... I'm trying to get the funds.");
        chat2.setImg(R.drawable.kenny);

        Chats chat3 = new Chats();
        chat3.setName("Beth Williams");
        chat3.setMsg("I'm looking for tips around capturing the milky way. I have a 6D with a 24-100mm...");
        chat3.setImg(R.drawable.william);

        Chats chat4 = new Chats();
        chat4.setName("Rev Shawn");
        chat4.setMsg("Wanted to ask  if you're available for a potrait shoot next week.");
        chat4.setImg(R.drawable.profile);

        array_chat.add(chat1);
        array_chat.add(chat2);
        array_chat.add(chat3);
        array_chat.add(chat4);

        chatAdapter = new ChatAdapter(this, array_chat);

        rv = findViewById(R.id.recyclerView);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        rv.setLayoutManager(linearLayoutManager);
        rv.setAdapter(chatAdapter);

    }
}
